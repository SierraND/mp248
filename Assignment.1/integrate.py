import numpy as np
import math

def sym_gauss_int_sqr(xb, n):
    """This uses the trapezoidal rule to integrate the Gaussian function by approximating the curve under the graph using right trapezoids. This uses input parameters, xb, which is the integration interval, and n, which is the number of grid cells."""
    #Set up the integration interval
    s=-xb
    f=xb
    n=n
    #Gaussian Function
    xvalues=np.linspace(s,f,n)
    a=1
    b=0
    c=math.sqrt(0.5)
    numerator=(xvalues-b)**2
    denominator=(2*c**2)
    yvalues=a*np.exp(-numerator/denominator)
    #Left hand side of equation
    deltax=(f-s)/(2*n)
    #Right hand side of equation
    yvalues=yvalues*2
    yvalues[0]=yvalues[0]/2
    yvalues[9]=yvalues[9]/2
    thesum=np.sum(yvalues)
    finalsum=deltax*thesum
    return finalsum