chmod +x shake_analysis.sh
#This gives how many lines are contained in the text file
awk 'END {print NR}' shakespeare-macbeth-46.txt
#This gives how many lines contain the string LADY MACBETH
awk '/LADY MACBETH/{count++} END{print count}' shakespeare-macbeth-46.txt
#This gives how many lines contain the strings LADY MACBETH and must
awk '/LADY MACBETH/&&/must/{count++} END{print count}' shakespeare-macbeth-46.txt
#This gives how many lines contain the string LADY MACBETH and blood (for reference)
#awk '/LADY MACBETH/&&/blood/' shakespeare-macbeth-46.txt
#This finds the line containing the two strings and prints out the 9th word
awk '/LADY MACBETH/&&/blood/{print $8}' shakespeare-macbeth-46.txt
#Create a file named shake.txt and replace LADY MACBETH with LADY GAGA
cp shakespeare-macbeth-46.txt shake.txt
sed -i 's/LADY MACBETH/LADY GAGA/g' shake.txt
