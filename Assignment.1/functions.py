import math
def gauss(x, a=1, b=0, c=1):
    """This is the Gaussian Function.
    It takes in the parameter x and uses the default values
    a=1
    b=0
    c=1
    to output the computed function."""
    return a*math.exp(-((x-b)**2)/(2*c**2))
